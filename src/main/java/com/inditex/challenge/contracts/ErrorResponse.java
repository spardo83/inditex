package com.inditex.challenge.contracts;

import lombok.Builder;
import lombok.Getter;

import java.util.Map;

@Getter
@Builder
public class ErrorResponse {
    private final Integer status;
    private final String message;
    private final String error;
    private final Map<String, String> fields;
}
