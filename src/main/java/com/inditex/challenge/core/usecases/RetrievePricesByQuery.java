package com.inditex.challenge.core.usecases;

import com.inditex.challenge.core.entities.Price;
import lombok.Builder;
import lombok.Getter;

import java.util.Date;
import java.util.List;
import java.util.function.Function;

public interface RetrievePricesByQuery extends Function<RetrievePricesByQuery.Model, List<Price>> {

    @Getter
    @Builder
    class Model {
        private final Date date;
        private final Long brandId;
        private final Long productId;
    }
}
