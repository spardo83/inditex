package com.inditex.challenge.core.usecases.impl;

import com.inditex.challenge.core.entities.Price;
import com.inditex.challenge.core.repositories.PriceRepository;
import com.inditex.challenge.core.usecases.RetrievePricesByQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
public class RetrievePricesByQueryImpl implements RetrievePricesByQuery {
    private final PriceRepository priceRepository;

    @Autowired
    public RetrievePricesByQueryImpl(PriceRepository priceRepository) {
        this.priceRepository = priceRepository;
    }

    @Override
    public List<Price> apply(Model model) {
        List<Price> prices = priceRepository.findAllByDateProductAndBrandOrderByPriorityDesc(model.getDate(), model.getProductId(), model.getBrandId());

        Set<Long> distinct= ConcurrentHashMap.newKeySet();

        return prices.stream()
                .filter(price-> distinct.add(price.getProductId()))
                .collect(Collectors.toList());
    }
}
