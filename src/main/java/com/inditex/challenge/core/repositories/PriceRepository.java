package com.inditex.challenge.core.repositories;

import com.inditex.challenge.core.entities.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface PriceRepository extends JpaRepository<Price, Long> {

    @Query("SELECT p FROM Price p  WHERE (p.startDate <= ?1 AND p.endDate >= ?1) AND p.productId = ?2 AND p.brandId = ?3 ORDER BY p.priority DESC")
    List<Price> findAllByDateProductAndBrandOrderByPriorityDesc(Date date, Long productId, Long brandId);
}
