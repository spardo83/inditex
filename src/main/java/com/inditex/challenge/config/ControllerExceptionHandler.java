package com.inditex.challenge.config;

import com.inditex.challenge.contracts.ErrorResponse;
import com.inditex.challenge.exceptions.ApiRestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ControllerExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @ExceptionHandler(value = {ApiRestException.class})
    protected ResponseEntity<ErrorResponse> handleApiException(ApiRestException e) {
        ErrorResponse errorResponse = ErrorResponse.builder()
                .error(e.getCode())
                .status(e.getStatusCode())
                .message(e.getMessage())
                .build();

        return ResponseEntity.status(errorResponse.getStatus())
                .body(errorResponse);
    }

    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<ErrorResponse> handleUnknownException(Exception e) {
        LOGGER.error("Internal error", e);
        ErrorResponse errorResponse = ErrorResponse.builder()
                .error("internal_error")
                .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                //This assumes that this is a public API where we don't must show any possible confidential info
                //if that's not the case replace with .message(ex.getMessage())
                .message("Something was wrong in our side, please try again in a few minutes. We are working in a solution.")
                .build();
        return ResponseEntity.status(errorResponse.getStatus())
                .body(errorResponse);
    }

    @ExceptionHandler(value = {HttpRequestMethodNotSupportedException.class, HttpMessageNotReadableException.class, MissingServletRequestParameterException.class})
    public ResponseEntity<ErrorResponse> badRequestException(HttpServletRequest req, Exception ex) {
        LOGGER.error("BAD REQUEST: {}, uri {}, message {}", ex.getClass().getSimpleName(), req.getRequestURI(), ex.getMessage(), ex);

        ErrorResponse errorResponse = ErrorResponse.builder()
                .error(ex.getClass().getSimpleName())
                .message(ex.getMessage())
                .status(HttpStatus.BAD_REQUEST.value())
                .build();
        return ResponseEntity.status(errorResponse.getStatus()).body(errorResponse);
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<ErrorResponse> noHandlerFoundException(HttpServletRequest req, NoHandlerFoundException ex) {
        ErrorResponse errorResponse = ErrorResponse.builder()
                .error("route_not_found")
                .message(String.format("Route %s not found", req.getRequestURI()))
                .status(HttpStatus.NOT_FOUND.value())
                .build();
        return ResponseEntity.status(errorResponse.getStatus())
                .body(errorResponse);
    }


}
