package com.inditex.challenge.exceptions;

import org.springframework.http.HttpStatus;

public class BadRequestException extends ApiRestException {
    protected BadRequestException(String code, String message) {
        super(code, message, HttpStatus.BAD_REQUEST.value());
    }

    protected BadRequestException(String message) {
        super(message, HttpStatus.BAD_REQUEST.value());
    }
}
