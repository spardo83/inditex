package com.inditex.challenge.exceptions;

import lombok.Getter;

@Getter

abstract public class ApiRestException extends RuntimeException {
    protected final String code;
    protected final String message;
    protected final Integer statusCode;


    protected ApiRestException(String code, String message, Integer statusCode) {
        this.code = code;
        this.message = message;
        this.statusCode = statusCode;
    }

    protected ApiRestException(String message, Integer statusCode) {
        this.code = "unknown_error";
        this.message = message;
        this.statusCode = statusCode;
    }
}
