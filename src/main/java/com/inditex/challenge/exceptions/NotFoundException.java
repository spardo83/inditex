package com.inditex.challenge.exceptions;

import org.springframework.http.HttpStatus;

public class NotFoundException extends ApiRestException {
    protected NotFoundException(String code, String message) {
        super(code, message, HttpStatus.NOT_FOUND.value());
    }

    protected NotFoundException(String message) {
        super(message, HttpStatus.NOT_FOUND.value());
    }
}
