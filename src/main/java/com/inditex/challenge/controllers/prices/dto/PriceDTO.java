package com.inditex.challenge.controllers.prices.dto;

import com.inditex.challenge.core.entities.Price;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.jackson.Jacksonized;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Builder
@Jacksonized
public class PriceDTO {
    private Long brandId;
    private Date startDate;
    private Date endDate;
    private Long priceList;
    private Long productId;
    private BigDecimal price;
    private String currency;

    public static PriceDTO fromPrice(Price price) {
        return PriceDTO.builder()
                .brandId(price.getBrandId())
                .startDate(price.getStartDate())
                .endDate(price.getEndDate())
                .priceList(price.getPriceList())
                .productId(price.getProductId())
                .price(price.getPrice())
                .currency(price.getCurrency())
                .build();
    }
}
