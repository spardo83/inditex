package com.inditex.challenge.controllers.prices.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.util.List;

@Getter
@Builder
@Jacksonized
public class RetrievePriceResponse {
    private List<PriceDTO> prices;
}
