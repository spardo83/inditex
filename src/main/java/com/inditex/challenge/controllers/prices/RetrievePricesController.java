package com.inditex.challenge.controllers.prices;

import com.inditex.challenge.controllers.prices.dto.PriceDTO;
import com.inditex.challenge.controllers.prices.dto.RetrievePriceResponse;
import com.inditex.challenge.core.entities.Price;
import com.inditex.challenge.core.usecases.RetrievePricesByQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.inditex.challenge.config.SpringConfig.DATE_FORMAT;

@RestController
@Validated
public class RetrievePricesController extends PricesBaseController {

    private final RetrievePricesByQuery retrievePricesByQuery;

    @Autowired
    public RetrievePricesController(RetrievePricesByQuery retrievePricesByQuery) {
        this.retrievePricesByQuery = retrievePricesByQuery;
    }

    @GetMapping("")
    public ResponseEntity<RetrievePriceResponse> retrieve(
            @RequestParam("date") @DateTimeFormat(pattern = DATE_FORMAT) Date date,
            @RequestParam("brand_id") Long brandId,
            @RequestParam("product_id") Long productId) {

        List<Price> result = retrievePricesByQuery.apply(RetrievePricesByQuery.Model.builder()
                .date(date)
                .brandId(brandId)
                .productId(productId)
                .build());

        List<PriceDTO> prices = result.stream()
                .map(PriceDTO::fromPrice)
                .collect(Collectors.toList());

        return ResponseEntity.ok(RetrievePriceResponse.builder()
                .prices(prices)
                .build());
    }


}
