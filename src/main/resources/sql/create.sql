DROP TABLE IF EXISTS `prices`;

CREATE TABLE `prices`
(
    `id`         BIGINT(20) NOT NULL AUTO_INCREMENT,
    `brand_id`   BIGINT(20) NOT NULL,
    `start_date` DATETIME   NOT NULL,
    `end_date`   DATETIME   NOT NULL,
    `price_list` BIGINT(20) NOT NULL,
    `product_id` BIGINT(20) NOT NULL,
    `priority`   INT        NOT NULL,
    `price`      DECIMAL    NOT NULL,
    `currency`   VARCHAR(3) NOT NULL,
    PRIMARY KEY (`id`)
);

INSERT INTO `prices` (`brand_id`, `start_date`, `end_date`, `price_list`, `product_id`, `priority`, `price`,
                      `currency`)
VALUES (1, '2020-06-14 00.00.00', '2020-12-31 23.59.59', 1, 35455, 0, 35.50, 'EUR'),
       (1, '2020-06-14 15.00.00', '2020-06-14 18.30.00', 2, 35455, 1, 25.45, 'EUR'),
       (1, '2020-06-15 00.00.00', '2020-06-15 11.00.00', 3, 35455, 1, 30.50, 'EUR'),
       (1, '2020-06-15 16.00.00', '2020-12-31 23.59.59', 4, 35455, 1, 38.95, 'EUR')


