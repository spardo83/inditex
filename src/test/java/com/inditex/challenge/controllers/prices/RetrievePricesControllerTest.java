package com.inditex.challenge.controllers.prices;

import com.inditex.challenge.InditexChallengeApplication;
import com.inditex.challenge.controllers.prices.dto.RetrievePriceResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = InditexChallengeApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RetrievePricesControllerTest {

    @Autowired
    protected TestRestTemplate testRestTemplate;

    @BeforeEach
    void initMocks() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    public void test_challenge_1() {

        ResponseEntity<RetrievePriceResponse> responseEntity = this.testRestTemplate.getForEntity("/inditex/prices?date=2020-06-14T10:00:00Z&brand_id=1&product_id=35455", RetrievePriceResponse.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody().getPrices().size()).isEqualTo(1);
    }

    @Test
    public void test_challenge_2() {
        ResponseEntity<RetrievePriceResponse> responseEntity = this.testRestTemplate.getForEntity("/inditex/prices?date=2020-06-14T16:00:00Z&brand_id=1&product_id=35455", RetrievePriceResponse.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody().getPrices().size()).isEqualTo(1);
    }

    @Test
    public void test_challenge_3() {
        ResponseEntity<RetrievePriceResponse> responseEntity = this.testRestTemplate.getForEntity("/inditex/prices?date=2020-06-14T21:00:00Z&brand_id=1&product_id=35455", RetrievePriceResponse.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody().getPrices().size()).isEqualTo(1);
    }

    @Test
    public void test_challenge_4() {
        ResponseEntity<RetrievePriceResponse> responseEntity = this.testRestTemplate.getForEntity("/inditex/prices?date=2020-06-15T10:00:00Z&brand_id=1&product_id=35455", RetrievePriceResponse.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody().getPrices().size()).isEqualTo(1);
    }

    @Test
    public void test_challenge_5() {
        ResponseEntity<RetrievePriceResponse> responseEntity = this.testRestTemplate.getForEntity("/inditex/prices?date=2020-06-16T21:00:00Z&brand_id=1&product_id=35455", RetrievePriceResponse.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody().getPrices().size()).isEqualTo(1);
    }
}